## THIS REPO IS DEPRICATED

Please base file export on apstools file writer base classes instead which are easier to understand maintain and use. https://bcda-aps.github.io/apstools/dev/examples/index.html#file-writers They export data promptly at the end of a run. 


# suitcase.specfile

This is a suitcase subpackage for writing a particular file format.

## Installation

```
pip install suitcase-specfile
```
